FROM php:8.2-fpm

WORKDIR /var/www/html

COPY .env.example .env
COPY . ./

RUN apt-get update && apt-get install -y \
    libzip-dev \
    zip \
    && docker-php-ext-configure zip \
    && docker-php-ext-install zip pdo pdo_mysql

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

COPY entrypoint.sh /usr/local/bin/entrypoint.sh
RUN chmod +x /usr/local/bin/entrypoint.sh

RUN composer install --no-scripts --no-autoloader

RUN composer dump-autoload --optimize

RUN php artisan key:generate

EXPOSE 8000

CMD ["entrypoint.sh"]
