<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Blog</title>
    
    <style>
    .main-navigation {
        background: #1d1d1d;
        clear: both;
        display: block;
        position: relative;
        width: 100%;
        z-index: 3;
        text-align: center; /* Centraliza o conteúdo horizontalmente */
    }

    .main-navigation ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
    }

    .main-navigation li {
        display: inline-block;
    }

    .main-navigation a {
        display: block;
        padding: 20px 30px; /* Ajuste o padding conforme necessário */
        text-decoration: none;
        color: #fff;
        transition: background-color 0.3s ease;
    }

    .main-navigation a:hover {
        background-color: #333; /* Mudança de cor ao passar o mouse */
    }
    
    body {
        font-family: Arial, sans-serif;
        line-height: 1.6;
        background-color: #f9f9f9;
        margin: 0;
        padding: 0;
    }
    .container {
        max-width: 1000px;
        margin: 20px auto;
        background-color: #fff;
        padding: 20px;
        border-radius: 5px;
        box-shadow: 0 2px 4px rgba(0,0,0,0.1);
    }
    h1 {
        color: #333;
        text-align: center;
    }
    p {
        color: #666;
    }
    .post-date {
        color: #999;
        font-size: 0.8em;
    }
</style>

</head>
<body>
    <header>
        <nav class="main-navigation">
            <ul>
                <li><a href="">Página Inicial</a></li>
                <li><a href="">Sobre</a></li>
                <li><a href="#">Contato</a></li>
            </ul>
        </nav>
    </header>
    <main>
        <div class="container">
            @yield('content')
        </div>
    </main>
</body>
</html>
