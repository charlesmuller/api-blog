<nav class="main-navigation">
    <ul>
        <li><a href="{{ route('home') }}">Home</a></li>
        <li><a href="{{ route('posts.index') }}">Posts</a></li>
        <li><a href="{{ route('old-posts.index') }}">Posts Antigos</a></li>
    </ul>
</nav>