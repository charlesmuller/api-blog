@extends('layout')

@section('content')
    <h1>{{ $sobre->post_title }}</h1>
    <p>{{ $sobre->post_content }}</p>
@endsection