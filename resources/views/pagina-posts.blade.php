@section('content')

<h1>Posts Anteriores</h1>
<ul>
    @foreach ($posts['posts'] as $post)
            <li> <a href="{{ route('getPosts', ['idPost' => $post->ID]) }}" target="_blank">{{ $post->post_title }}</a> 
                <ul> <a>{{ $post->post_date }}</a> </ul>
            </li>
    @endforeach

</ul>

<div style="padding: 5%; display: flex; justify-content: space-evenly">
    @if ($posts['posts']->currentPage() > 1)
    <a href="{{ $posts['posts']->url($posts['posts']->currentPage() - 1) }}" class="btn btn-primary" style="width: 8%;">
        <img src="previous-page.svg" alt="Página Anterior" style="width: 100%; height: 30px;">
    </a>
    @else
        <a href="{{ $posts['posts']->url($posts['posts']->currentPage() - 1) }}" class="btn btn-primary" style="width: 30px; height: 30px; background-image: url('previous-page.svg'); background-size: cover;"></a>
    @endif

    @foreach(range(1, $posts['posts']->lastPage()) as $page)
        @if ($page == $posts['posts']->currentPage())
            <a href="{{ $posts['posts']->url($page) }}" style="color: red;">{{ $page }}</a>
        @else
            <a href="{{ $posts['posts']->url($page) }}">{{ $page }}</a>
        @endif
    @endforeach

    <a href="{{ $posts['nextPageCursor'] }}" class="btn btn-primary" style="width: 8%;">
        <img src="next-page.svg" alt="Próxima Página" style="width: 100%; height: 30px;">
    </a>
</div>

@endsection

