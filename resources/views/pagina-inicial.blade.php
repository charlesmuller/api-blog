@extends('layout')

@section('content')
    @foreach ($posts as $post)
        <h1>{{ $post->post_title }}</h1>
        <p>{{ $post->post_content }}</p>
        <p>{{ $post->post_date }}</p>
    @endforeach
@endsection