<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Meu Blog</title>
    <!-- Adicione seus estilos CSS aqui -->
</head>
<body>

    <main>
        @yield('content')
    </main>

    <!-- Adicione seus scripts JavaScript aqui -->

</body>
</html>
