@extends('layouts.app')

@section('content')
    @include('menu')
    <div class="container">
        <h1>{{ $post->post_title }}</h1>
        <p style="text-align: justify;" >{{ strip_tags($post->post_content) }}</p>
        <p class="post-date">Data de Atualização: {{ $post->post_modified }}</p>
    </div>
@endsection
