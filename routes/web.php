<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\PageController;

Route::redirect('/', '/home');


// Route::get('/posts/{idPost}', [PostController::class, 'getPosts'])->name('getPosts');
Route::get('/home', [PageController::class, 'getHome'])->name('home');
// Route::get('/sobre', [PageController::class, 'getAboutPage'])->name('getAboutPage');
// Route::get('/pagina-posts', [PageController::class, 'getPagePosts'])->name('getPagePosts');


Route::get('/posts/{id}', [PostController::class, 'getPosts'])->name('posts.index');

Route::get('/allPosts', [PostController::class, 'getAllPosts'])->name('posts.index');

Route::get('/old-posts', [PostController::class, 'indexAll'])->name('old-posts.index');