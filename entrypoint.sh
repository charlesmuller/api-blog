#!/bin/bash

set -e

# Generate Laravel application key if not already set
if [ ! -f /var/www/html/.env ]; then
    cp /var/www/html/.env.example /var/www/html/.env
fi

if ! grep -q "APP_KEY=" /var/www/html.env; then
    php /var/www/html/artisan key:generate --ansi
fi

# Start Laravel server
exec php /var/www/html/artisan serve --host=0.0.0.0
