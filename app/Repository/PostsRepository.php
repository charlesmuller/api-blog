<?php
namespace App\Repository;

use Illuminate\Support\Facades\DB;

class PostsRepository
{
    public function buscaPosts($idPost)
    {
        $idPost = intval($idPost);
        $post = DB::table('posts')->where('ID', $idPost)->first();
        return $post;
    }

    public function getPaginatedPosts($perPage)
    {
        $posts = DB::table('posts')->paginate($perPage);
        
        // $nextPageCursor = $post->nextPageUrl();

        // return ['posts' => $post, 'nextPageCursor' => $nextPageCursor];
        return $posts;

    }

    public function getSobre()
    {
        return DB::table('posts')->where('ID', 2)->first();
    }

    public function buscaTodosPosts()
    {
        $posts = DB::table('posts')->orderBy('post_date', 'ASC');
        // return view('old-posts.index', ['posts' => $posts]);
        return $posts;
    }
}


        // ->where('post_status', 'publish')
        // ->where('post_type','!=', 'customize_changeset')
        // ->where('post_type','!=', 'wp_global_styles')
        // ->where('post_name','!=', 'about')
        // ->orderByDesc('ID')
        // ->paginate($perPage);