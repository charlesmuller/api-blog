<?php

namespace App\Http\Controllers;
use App\Repository\PostsRepository;

class PageController extends Controller
{
    protected $postsRepository;

    public function __construct(PostsRepository $postsRepository)
    {
        $this->postsRepository = $postsRepository;
    }

    public function getHome()
    {
        $posts = $this->postsRepository->getPaginatedPosts(10);
        $oldPosts = $this->postsRepository->buscaTodosPosts();

        return view('home')->with([
            'posts' => $posts, 
            'oldPosts' => $oldPosts
            ]);
    }

    public function redirectToFirstPage()
    {
        return redirect()->route('getFirstPage');
    }

    public function getAboutPage()
    {
        $sobre = $this->postsRepository->getSobre();
        return view('sobre', compact('sobre'));
    }

    public function getPagePosts()
    {
        $posts = $this->postsRepository->getPaginatedPosts(10);
        return view('pagina-posts', compact('posts'));
    }
}
