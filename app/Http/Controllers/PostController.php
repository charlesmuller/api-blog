<?php

namespace App\Http\Controllers;
use App\Models\PostsModel;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function __construct() 
    {
        $this->model = new PostsModel();
    }

    public function getPosts(Request $request)
    {
        $idPost = $request->id;
        $idPostInt = intval($idPost);
        if(is_numeric($idPost) && $idPostInt > 0) {
            $post = $this->model->verificaSeExistePost($idPost);

            return view('posts.index', ['post' => $post, 'idPost' => $idPost])->with('layout', 'layouts.app');
        }

        return response()->json(['error' => 'ID de post inválido'], 400);
    }

    public function getAllPosts()
    {
        $posts = PostsModel::all();
        return view('pagina-inicial', compact('posts'));
    }

    public function index()
    {
        // Lógica para obter os posts recentes
        return view('posts.index');
    }

    public function indexAll()
    {
        // Lógica para obter todos os posts (recentes e antigos)
        return view('old-posts.index');
    }

}
