<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Repository\PostsRepository;

class PostsModel extends Model
{
    use HasFactory;
    protected $table = 'posts';

    public function __construct()
    {
        $this->repository = new PostsRepository();
    }
    public function verificaSeExistePost($idPost)
    {
        return $this->repository->buscaPosts($idPost);
    }
}
